mariadb
==========

Ansible playbook that installs and configures mariadb on Debian

Role Variables
--------------

The desired behavior can be refined via variables.

Option | Description
--- | ---
`mariadb_packages` | A list of mariadb packages to install.
`mariadb_root_password` | Password of the mariadb root user.

The following `mariadb_packages` are defined by default:

```yaml
  - python-mysqldb
  - mariadb-client
  - mariadb-server
```

Example Playbook
----------------

The following will install wget and update all packages on your local host.

```yaml
# file: test.yml
- hosts: local

  vars:
    mariadb_root_password: 'ThisIsMySuperSecurePassword98765'
        
  roles:
    - mariadb
```